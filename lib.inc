section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop_size:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop_size
    .end
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    push 0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        cmp rax, 0
        je .print
        jmp .loop

    .print:
        mov rdi, rsp
	push rdi
        call print_string
	pop rdi
        mov rsp, r9
    .end
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    .loop_cmp:
        mov rdx, [rsi + rax]
        cmp byte [rdi + rax], dl
        jne .end_false
        cmp byte [rdi + rax], 0
        je .end_true
        inc rax
        jmp .loop_cmp
    .end_true:
        mov rax, 1
        ret
    .end_false:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor r8, r8
    .loop_read_word:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
    .is_space:
        cmp rax, ' '
        je .is_first_space
        cmp rax, '	'
        je .is_first_space
        cmp rax, '\n'
        je .is_first_space
    .not_space:
        cmp rax, 0
        je .last_space
        cmp r8, rsi
        je .end_false
        mov [rdi+r8], rax
        inc r8
        jmp .loop_read_word
    .is_first_space:
        cmp r8, 0
        je .loop_read_word
    .last_space:
        cmp r8, rsi
        je .end_false
        mov [rdi+r8], byte 0
        jmp .end_true
    .end_false:
        mov rax, 0
        ret
    .end_true:
        mov rax, rdi
        mov rdx, r8
        ret    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10
    xor r9, r9
    .is_number:
        mov r9b, byte [rdi + rcx]
        cmp r9b, '0'
        jb .end
        cmp r9b, '9'
        ja .end
    .add_number:
        inc rcx
        sub r9b, '0'
        mul r8
        add rax, r9
        jmp .is_number
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r8b, byte[rdi]
    cmp r8b, '-'
    je .neg
    jmp parse_uint
    .neg:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        neg rax
        inc rdx
    .end:
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdi
    push rdx
    push rsi
    call string_length
    pop rsi
    pop rdx
    pop rdi
    cmp rax, rdx
    jae .overflow
    xor r8, r8
    .loop_copy_string:
        xor r9, r9
        mov r9b, byte [rdi + r8]
        mov byte[rsi + r8], r9b
        cmp r9b, 0
        je .end
        inc r8
        jmp .loop_copy_string
    .end:
        ret
    .overflow:
        xor rax, rax
        ret
